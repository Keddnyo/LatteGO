<img src="https://raw.githubusercontent.com/Keddnyo/LatteGO/master/app/src/main/res/mipmap-xxxhdpi/ic_launcher.png" align="right"/>

# LatteGO
 MI Pad 2 tablet advanced boot options
## Features
* Hidden target device checker
* Simple GUI
* Reboot options
  * Shutdown
  * Recovery
  * Fastboot
  * DNX
  * Reboot
  * Sleep
  * Safe mode
  * Windows *(Only via shortcut)*
* Reboot confirmation dialog
## Hidden features
  * Long click on "**DNX**" make an attempt to fix EFI partition
  * Long click on "**Recovery**" enables Recovery shortcut
  * Long click on "**Reboot**" enables Windows shortcut
  * Long click on "**Safe mode**" disables Windows shortcut
  * Long click on "**Power off**" disables Recovery shortcut
